import Vue from 'vue';
import Vuex from 'vuex';
import {
  fetchUsers,
  fetchAlbums,
  fetchPhotos,
} from '@/api';


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data: [],
  },
  getters: {},
  actions: {
    getUsers({ commit }) {
      return new Promise((resolve, reject) => {
        fetchUsers()
          .then((response) => {
            const endpoint = response.data;
            // console.log(endpoint)

            commit('SET_USERS', endpoint);
            resolve(response);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },
    getAlbums({ commit }) {
      return new Promise((resolve, reject) => {
        fetchAlbums()
          .then((response) => {
            const endpoint = response.data;
            // console.log(endpoint)

            commit('SET_ALBUMS', endpoint);
            resolve(response);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },

    getPhotos({ commit }) {
      return new Promise((resolve, reject) => {
        fetchPhotos()
          .then((response) => {
            const endpoint = response.data;
            // console.log(endpoint)

            commit('SET_PHOTOS', endpoint);
            resolve(response);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },
  },
  mutations: {
    SET_USERS(state, data) {
      Vue.set(state, 'data', data);
    },
    SET_ALBUMS(state, data) {
      Vue.set(state, 'data', data);
    },
    SET_PHOTOS(state, data) {
      Vue.set(state, 'data', data);
    },
  },
});
