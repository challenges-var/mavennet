import { HTTP } from './http';

/* eslint-disable */
export const fetchUsers = () => new Promise((resolve, reject) => HTTP.get(`https://jsonplaceholder.typicode.com/users`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));

export const fetchAlbums = () => new Promise((resolve, reject) => HTTP.get(`https://jsonplaceholder.typicode.com/albums`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));

export const fetchPhotos = () => new Promise((resolve, reject) => HTTP.get(`https://jsonplaceholder.typicode.com/photos`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));
